
//
// Model a simple light
//

// Ensure matrices are row-major
#pragma pack_matrix(row_major)


//-----------------------------------------------------------------
// Structures and resources
//-----------------------------------------------------------------

cbuffer sceneCBuffer : register(b3) {
	float4				windDir;
	float				Time;
	float				grassHeight;
	float				dayNightBlendVal;
};

//
// Textures
//

// Assumes texture bound to texture t0 and sampler bound to sampler s0
TextureCube dayCubeMap : register(t0);
TextureCube nightCubeMap : register(t1);
SamplerState linearSampler : register(s0);


//-----------------------------------------------------------------
// Input / Output structures
//-----------------------------------------------------------------

// Input fragment - this is the per-fragment packet interpolated by the rasteriser stage
struct FragmentInputPacket {


	float3				texCoord		: TEXCOORD;
	float4				posH			: SV_POSITION;
};


struct FragmentOutputPacket {

	float4				fragmentColour : SV_TARGET;
};


//-----------------------------------------------------------------
// Pixel Shader - Lighting 
//-----------------------------------------------------------------

FragmentOutputPacket main(FragmentInputPacket v) { 
	float3 colour = float3(0.0, 0.0, 0.0);
	float alpha = 1.0f;

	float srcblendFactor = 1.0f - dayNightBlendVal;
	float dstblendFactor = dayNightBlendVal;

	//blend the two textures of day and night together based off the timer value
	colour = (dayCubeMap.Sample(linearSampler, v.texCoord) * srcblendFactor).xyz + (nightCubeMap.Sample(linearSampler, v.texCoord).xyz * dstblendFactor);
	
	FragmentOutputPacket outputFragment;

	outputFragment.fragmentColour = float4(colour, alpha);

	return outputFragment;
}
